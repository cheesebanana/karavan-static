=======================================================================================================================
= GRUNT BASED PROJECT
=======================================================================================================================
= v. 0.0.11
=======================================================================================================================
= (only useful catalogs)
=======================================================================================================================


{swig}
  |
  -- {blocks}
  |     |
  |     -- 'some-block.swig' --> block template
  |     |
  |     -- 'some-block.view' --> block view (needs for testing block template) - rendered to html
  |
  -- {pages}
  |     |
  |     -- 'some-page.swig' --> page template - rendered to html
  |
  -- 'layout.swig'  --> base for 'some-block.view' and 'some-page.swig'
  |
  -- 'blocks.swig'  --> automatically created by concating all blocks from {swig}/{blocks}

{html}
  |
  -- {blocks}
  |     |
  |     -- 'some-block.view.html'  --> automatically created by html-rendering 'some-block.view' from {swig}/{blocks}
  |
  -- {pages}
        |
        -- 'some-page.html'  --> automatically created by html-rendering 'some-page.swig' from {swig}/{pages}

{scss}
  |
  -- {base}
  |     |
  |     -- 'some-base-file.scss'  --> for example, normalize.scss, grid.scss, bootstrap.scss, etc.
  |
  -- {style}
  |     |
  |     -- 'main.scss'  --> scss-container for all your scss-stuff
  |     |
  |     -- 'some-block.scss'  --> styles for some block from {swig}/{blocks}
  |     |
  |     -- 'some-page.scss'  --> styles for some page from {swig}/{pages}
  |
  -- 'base.scss'  --> automatically created by concating all styles from {scss}/{base}
  |
  -- 'style.scss'  --> automatically created by concating all styles from {scss}/{style}

{css}
  |
  -- 'base.css'  --> automatically created by compass from 'base.scss'
  |
  -- 'style.css'  --> automatically created by compass from 'style.scss'

{js}
  |
  -- {plugins}
  |     |
  |     -- 'some-plugin.js'  --> NOT minified version of some js-plugin
  |
  -- {script}
  |     |
  |     -- 'main.js'  --> js-container for all your js-stuff
  |     |
  |     -- 'some-block.js'  --> script for some block from {swig}/{blocks}
  |     |
  |     -- 'some-page.js'  --> script for some page from {swig}/{pages}
  |
  -- 'plugins.js'  --> automatically created by concating all scripts from {js}/{plugins}
  |
  -- 'plugins.min.js'  --> minified version of 'plugins.js'
  |
  -- 'script.js'  --> automatically created by concating all scripts from {js}/{script}
  |
  -- 'script.min.js'  --> minified version of 'script.js'

{images}
